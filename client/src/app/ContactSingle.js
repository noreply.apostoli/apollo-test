import React, { Component } from "react";
import { graphql } from "react-apollo";
import gql from "graphql-tag";
import NoteList from './NoteList';
import ContactHeader from './ContactHeader';
import AddNote from './AddNote';

class ContactSingle extends Component {

  componentWillMount() {
    this.props.data.subscribeToMore({
      document: noteSubscription,
      variables: {
        contactId: this.props.match.params.id,
      },
      updateQuery: (prev, {subscriptionData}) => {
        if (!subscriptionData.data) {
          return prev;
        }

        const newNote = subscriptionData.data.noteAdded;
        if(!prev.contact.notes.find((item) => item.id === newNote.id)) {
          return Object.assign({}, prev, {
            contact: Object.assign({}, prev.contact, {
              notes: [...prev.contact.notes, newNote]
            })
          });
        } else {
          return prev;
        }
      },
    });
  }

  render() {
    const { data: { loading, error, contact }, match } = this.props;
    if (loading) {
      return <ContactHeader contactId={match.params.id}/>;
    }

    if (error) {
      return <p>{error.message}</p>;
    }

    if (contact.notes === null) {
      return (
        <div className="container">
          <h2>{contact.firstName} {contact.lastName}</h2>
        <AddNote />
      </div>
      );
    }

    return (
      <div className="container">
      <div className="row"><h2>{contact.firstName} {contact.lastName}</h2></div>
      <NoteList notes={contact.notes} />
      <AddNote />
    </div>
    );
  }
}

export const  noteSubscription = gql`
  subscription NoteAdded($contactId: ID!) {
    noteAdded(contactId: $contactId) {
      id
      details
    }
  }
`;

export const contactSingleQuery = gql`
  query contactSingleQuery($contactId: ID!) {
    contact(id: $contactId) {
      id
      firstName
      lastName
      notes {
        id
        details
      }
    }
  }
`;

export default graphql(contactSingleQuery, {
  options: (props) => ({
    variables: { contactId: props.match.params.id },
  }),
})(ContactSingle);
