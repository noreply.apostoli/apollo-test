import React, { Component } from "react";
import { ApolloClient } from "apollo-client";
import { split } from "apollo-link";
import { HttpLink } from "apollo-link-http";
import { WebSocketLink } from "apollo-link-ws";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloProvider, toIdValue } from "react-apollo";
import { getMainDefinition } from "apollo-utilities";
import { addGraphQLSubscription } from "subscriptions-transport-ws";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import Contacts from "./Contacts";
import AddContact from "./AddContact";
import ContactSingle from "./ContactSingle";

const PORT = 4000;

const httpLink = new HttpLink({ uri: "http://localhost:4000/graphql" });

const wsLink = new WebSocketLink({
  uri: `ws://localhost:${PORT}/subscriptions`,
  options: {
    reconnect: true
  }
});

// using the ability to split links, you can send data to each link
// depending on what kind of operation is being sent
const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === "OperationDefinition" && operation === "subscription";
  },
  wsLink,
  httpLink
);

const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache()
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <BrowserRouter>
          <div>
            <div className="navbar-fixed">
              <nav className="teal darken-1">
                <div className="nav-wrapper">
                  <Link to="/" className="brand-logo center">
                    CRM
                  </Link>
                </div>
              </nav>
            </div>
            <div className="App-intro">
              <AddContact />
              <Switch>
                <Route exact path="/" component={Contacts} />
                <Route path="/contact/:id" component={ContactSingle} />
              </Switch>
            </div>
          </div>
        </BrowserRouter>
      </ApolloProvider>
    );
  }
}

export default App;
