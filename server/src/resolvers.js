import { PubSub, withFilter } from 'graphql-subscriptions';
import getRepositoryByName from './github-connector';

const pubsub = new PubSub();

const contacts = [
  {
    id: '1',
    firstName: 'nome',
    lastName: 'cognome',
    notes: [
      { id: '1', details: 'chi???'},
      { id: '2', details: 'bha'},
    ],
  },
  {
    id: '2',
    firstName: 'nome2',
    lastName: 'cognome2',
    notes: [
      { id: '1', details: 'chi???'},
      { id: '2', details: 'bha'},
    ],
  },
  {
    id: '3',
    firstName: 'nome3',
    lastName: 'cognome3',
    notes: [
      { id: '1', details: 'chi???'},
      { id: '2', details: 'bha'},
    ],
  }
];

export const resolvers = {
  Query: {
    contacts: () => {
      return contacts;
    },
    contact: (root, { id }) => {
      return contacts.find(contact => contact.id === id);
    },
    gitHubRepository(root, args, context) {
      return getRepositoryByName(args.name);
    },
  },
  Mutation: {
    addContact: (root, args) => {
      const newContact = { id: args.id, firstName: args.firstName, lastName: args.lastName };
      contacts.push(newContact);
      return newContact;
    },
    addNote: (root, { note }) => {
      const newId = require('crypto').randomBytes(5).toString('hex');
      const contact = contacts.find(contact => contact.id === note.contactId);
      const newNote = { id: String(newId), details: note.details };
      contact.notes.push(newNote);
      pubsub.publish('noteAdded', { noteAdded: newNote, contactId: note.contactId });
      return newNote;
    },
  },
  Subscription: {
    noteAdded: {
      subscribe: withFilter(() => pubsub.asyncIterator('noteAdded'), (payload, variables) => {
        return payload.contactId === variables.contactId;
      }),
    },
  },
};
