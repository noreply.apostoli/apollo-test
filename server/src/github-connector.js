import rp from 'request-promise';

const getRepositoryByName = (name) => {
  return rp({
    uri: `https://api.github.com/users/${name}/repos`,
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true,
  });
}

export default getRepositoryByName;
